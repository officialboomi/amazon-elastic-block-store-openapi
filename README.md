# Amazon Elastic Block Store Connector
You can use the Amazon Elastic Block Store (Amazon EBS) direct APIs to create Amazon EBS snapshots, write data directly to your snapshots, read data on your snapshots, and identify the differences or changes between two snapshots.

Documentation: https://docs.aws.amazon.com/ebs/latest/APIReference/Welcome.html

Specification: https://github.com/APIs-guru/openapi-directory/blob/main/APIs/amazonaws.com/ebs/2019-11-02/openapi.yaml

## Prerequisites

+ Generate an AWS Acess Key ID and AWS Secret Key from the AWS Admin Console

## Supported Operations
All endpoints are operational.
## Connector Feedback

Feedback can be provided directly to Product Management in our [Product Feedback Forum](https://community.boomi.com/s/ideas) in the boomiverse.  When submitting an idea, please provide the full connector name in the title and a detailed description.

